<script type="text/javascript">
var jsonData = '{"estudiantes":[' +
'{"codigo":"001","nombre":"Pedro Ramirez","nota":100},' +
'{"codigo":"002","nombre":"Ana Marciano","nota":62},' +
'{"codigo":"003","nombre":"Jose Mendoza","nota":78},' +
'{"codigo":"004","nombre":"Teresa Hernandez","nota":95},' +
'{"codigo":"005","nombre":"Thomas Perdomo","nota":92},' +
'{"codigo":"006","nombre":"Gisele Colmenares","nota":75},' +
'{"codigo":"007","nombre":"Ruben Casanova","nota":48},' +
'{"codigo":"008","nombre":"Christina Zapata","nota":59},' +
'{"codigo":"009","nombre":"Daniel Perez","nota":88},' +
'{"codigo":"010","nombre":"Luisa Mota","nota":88}' + ']}';

var jsObj = JSON.parse(jsonData);


function datoEstudiantes(json, valor) {
var titulo = "<div class='indiv2'>Promedio de Notas </div>";
var titulo2 = "<div class='indiv2'>Mayor Nota</div>";
var titulo3 = "<div class='indiv2'>Menor Nota</div>";
var titulo1 = "<div class='indiv'> <h2>Estudiantes</h2> <p>Lista de estudiantes</p> </div>";
var out = "<div class='indiv'><p><span> Avatar</span></p><p><span>Nombre</span></p><p><span>Nota</span></p><p><span>Codigo</span></p></div>";
var i;
var suma = 0;
var arr = [];
for (i = 0; i < json.estudiantes.length; i++) {
/*Sentencia que lista los estudiantes*/
out += "<div class='indiv'>" + "<p><span><img src='img/" + json.estudiantes[i].codigo + ".png'></span></p>" + "<p><span>" + json.estudiantes[i].nombre + "</span></p>" + "<p><span>" + json.estudiantes[i].nota + "</span></p>" + "<p><span>" + json.estudiantes[i].codigo + "</span></p>" + "</div>";

/*Sentencia que suma las notas*/
suma += json.estudiantes[i].nota;

/*Convertir Json en arreglo*/
arr.push(json.estudiantes[i].nota);
}
/*con el nuevo spread operator ... podemos capturar el arreglo para aplicarle los metodos Math y asi obtener el mayor y menor numero del arreglo*/
var maxi = Math.max(...arr);
var mini = Math.min(...arr);

/* Dividimos la suma de las notas entre el cantidad de registros para obtener el promedio*/
var promed = suma / json.estudiantes.length;

/*Usamos condicional if else según corresponda*/

/*Corresponde al evento datoEstudiantes para listar todos los datos de estudiantes*/
if (valor == 'lista') {
document.getElementById("listEstudiantes").innerHTML = "<div class='Estudian'>" + titulo1 + out + "</div>";
document.getElementById("scroll").classList = "scroll";
/*document.getElementById("vver").style.display='flex';*/
}
/*Corresponde al evento datoEstudiantes para mostrar el promedio de las notas*/
else if (valor == 'pro') {
document.getElementById("promedio").innerHTML = titulo + "<div class='indiv2'>" + promed + "</div>";
}
/*Corresponde al evento datoEstudiantes para mostrar la nota maxima*/
else if (valor == "maxi") {
document.getElementById("mayor").innerHTML = titulo2 + "<div class='indiv2'>" + maxi + "</div>";
}
/*Corresponde al evento datoEstudiantes para mostrar la nota minima*/
else {
document.getElementById("menor").innerHTML = titulo3 + "<div class='indiv2'>" + mini + "</div>";
}


}
</script>