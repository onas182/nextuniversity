function sumar(){
    num1 = parseFloat(document.getElementById("num1").value);
    num2 = parseFloat(document.getElementById("num2").value);
    
    if (validar(num1, num2)){
        var res = num1 + num2;
        alert("la suma es: " + res);
    }else{
        alert("Formato incorrecto de numeros !!!");
    }
}

function restar(){
    
    num1 = parseFloat(document.getElementById("num1").value);
    num2 = parseFloat(document.getElementById("num2").value);
    
    if(validar(num1, num2)){
        res = num1 - num2;
        alert("La Resta es: " + res)
    }else{
        alert("Formato incorrecto de numeros !!!");
    }
}

function multiplicar(){

    num1 = parseFloat(document.getElementById("num1").value);
    num2 = parseFloat(document.getElementById("num2").value);

    if(validar(num1, num2)){
        res = num1 * num2;
        alert("La Multiplicacion es: " + res)
    }else{
        alert("Formato incorrecto de numeros !!!");
    }
}

function dividir(){

    num1 = parseFloat(document.getElementById("num1").value);
    num2 = parseFloat(document.getElementById("num2").value);

    if(validar(num1, num2)){
        res = num1 / num2;
        if (res==Infinity){
           alert("Division entre cero!");
            document.getElementById("res").value = "";
            
        }else{
            alert("La division es: " + res)
        }        
    }else{
        alert("Formato incorrecto de numeros !!!");
    }
}

function validar(num1, num2){
    if (isNaN(num1) || isNaN(num1)){
        
        return false;
    }else{
        return true;
    }
}
function hacerTabla(){
    
    tabla = "";
    num = parseFloat(document.getElementById("num").value);
    
    if (isNaN(num) == false) {
        tabla += 'TABLA DEL ' + num + '<br>';
        for (i = 0; i <= 10; i++){
            tabla+= num+" x "+ i + " = " + num * i + "<br>";
        }
        document.getElementById("tabla").innerHTML = tabla;
    }else{
        alert("Formato incorrecto de numero !!");
    }
}