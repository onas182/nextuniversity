/**
 * Created by Onas on 20/5/2016.
 */

var gulp = require('gulp'),
    connect =  require('gulp-connect');



gulp.task('connect', function() {
    connect.server({
        root: 'curso',
        livereload: true
    });
});

gulp.task('livereload', function () {
    gulp.src(['curso/**/*.html','curso/**/*.css','curso/**/*.js'])
        .pipe(connect.reload());
});

gulp.task('watch', function () {
    gulp.watch(['curso/**/*.html'], ['livereload']);
    gulp.watch(['curso/**/*.css'], ['livereload']);
    gulp.watch(['curso/**/*.js'], ['livereload']);
});

gulp.task('default', ['connect','livereload', 'watch']);
